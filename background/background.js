class Config
{
  /**
   * @callback Config~readCallback
   * @param {object} err An error object or undefined
   * @param {object} config The config
   */
  /**
   * Reads the config from user storage. If no config exists a default config will
   * be returned.
   * @param {readConfigCallback} callback
   */
  _read(callback)
  {
    chrome.storage.local.get('config', (items) =>
    {
      if (chrome.runtime.lastError)
      {
        console.log(`Error reading config:`);
        console.log(chrome.runtime.lastError);

        callback && callback(chrome.runtime.lastError, this._config);
      }
      else
      {
        if (items.config && (Object.keys(items.config).length > 0))
        {
          this._config = items.config;
        }

        this._readCallback && this._readCallback();
        callback && callback(undefined, this._config);
      }
    });
  }

  /**
   * @callback Config~writeCallback
   * @param {object} err An error object or undefined
   */
  /**
   * Writes the local configuration file to storage
   * @param {Config~writeCallback} callback 
   */
  _write(callback)
  {
    let writeConfig = JSON.parse(JSON.stringify(this._config));

    delete writeConfig.isNewVersionAvailable;

    chrome.storage.local.set({ 'config': writeConfig }, () =>
    {
      if (chrome.runtime.lastError)
      {
        callback && callback(chrome.runtime.lastError);
      }
      else
      {
        console.log('wrote config');
        callback && callback(undefined);
      }
    });
  }

  /**
   * Updates the configuration in memory
   * @param {object} cfg The new configuration
   */
  setConfig(cfg)
  {
    if (JSON.stringify(cfg) !== JSON.stringify(this._config))
    {
      const WRITE_DELAY = 60000;

      this._config = JSON.parse(JSON.stringify(cfg));

      console.log(`config has been modified, waiting ${WRITE_DELAY / 1000}s before writing`);

      if (this._writeTimeoutId !== undefined)
      {
        clearTimeout(this._writeTimeoutId);
        this._writeTimeoutId = undefined;
      }

      this._writeTimeoutId = setTimeout(() =>
      {
        this._write((err) =>
        {
          if (err)
          {
            console.log('Error writing config:');
            console.log(err);
          }
        });
      }, WRITE_DELAY);
    }
  }

  /**
   * @return The mutable configuration object 
   */
  getConfig()
  {
    return this._config;
  }

  /**
   * @callback Config~readCallback
   */
  /**
   * Constructor
   * @param {Config~readCallback} readCallback Gets called whenever the config
   *                                           has been reloaded from storage.
   */
  constructor(readCallback)
  {
    this._readCallback = readCallback;

    /**
     * Default configuration
     * @type {object}
     */
    this._config = {
      isNewVersionAvailable: false,
      filter: {
        maximumQuestionMarks: 2,
        minimumReputation: 0,
        blacklists: {
          forums: [ /* counterstrike-global-offensive, playerunknown-s-battlegrounds */],
          threads: [ /* 214976, 214830 */],
          members: [ /* 1690019, 168634 */],
        },
      },
      cache: {
        members: { /* '1690019': { reputation: 4 }, '168634': { reputation: 452 } */ },
        threads: { /* '214976': { op: { id: 1690019 } }, '214830': { op: { id: 168634 } } */ },
      },
    };

    /**
     * The id of the timeout used to wait for more config modifications before
     * writing the config to storage.
     * @type {number}
     */
    this._writeTimeoutId = undefined;

    this._read((err, cfg) =>
    {
      if (err)
      {
        console.log('Error reading config:');
        console.log(err);
      }
    });
  }
}

class Background
{
  /**
   * Handles browser runtime messages
   */
  _runConnector()
  {
    chrome.runtime.onMessage.addListener((request, sender, sendResponse) =>
    {
      if (chrome.runtime.lastError)
      {
        console.log('Error:');
        console.log(chrome.runtime.lastError);
      }

      if (request.config)
      {
        if (request.config.get)
        {
          sendResponse(this._config.getConfig());
        }
        else if (request.config.set)
        {
          this._config.setConfig(request.config.set);
        }
      }
      else
      {
        console.log(`Unknown extension request:`);
        console.log(request);
        console.log('by');
        console.log(sender);

        sendResponse();
      }
    });
  }

  /**
   * Checks for available updates
   */
  _checkUpdates()
  {
    web.get(`${links.git}/raw/master/manifest.json`, (data, status) =>
    {
      if (status === 'success')
      {
        let mf = undefined;

        try
        {
          mf = JSON.parse(data);
        }
        catch (ex)
        {
          console.log('Error checking for updates:');
          console.log(ex);
        }

        if (mf)
        {
          if (mf.version > chrome.runtime.getManifest().version)
          {
            this._isNewVersionAvailable = true;
            this._config.getConfig().isNewVersionAvailable = this._isNewVersionAvailable;
            chrome.browserAction.setBadgeText({ text: "UPD" });
          }
        }
      }
      else
      {
        console.log('Error checking for updates:');
        console.log(data);
      }
    });
  }

  constructor()
  {
    this._isNewVersionAvailable = false;

    this._config = new Config(() =>
    {
      this._config.getConfig().isNewVersionAvailable = this._isNewVersionAvailable;
    });

    this._checkUpdates();

    this._runConnector();
  }
}

new Background();

