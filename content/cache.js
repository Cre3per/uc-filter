class Cache
{
  /**
   * @callback Cache~getMemberCallback
   * @param {object} err An error object or undefined
   * @param {object} member The member object
   */
  /**
   * Loads member information from cache
   * @param {number} memberId The id of the member to get information about
   * @param {Cache~getMemberCallback} callback
   */
  getMember(memberId, callback)
  {
    if (this._config.cache.members[memberId] === undefined)
    {
      callback && callback({ msg: 'no information' }, undefined);
    }
    else
    {
      callback && callback(undefined, this._config.cache.members[memberId]);
    }
  }

  /**
   * @callback Cache~getThreadCallback
   * @param {object} err An error object or undefined
   * @param {object} thread The thread object
   */
  /**
   * Loads thread information from cache or downloads and stores them
   * @param {number} threadId The id of the thread to get information about
   * @param {Cache~getThreadCallback} callback
   */
  getThread(threadId, callback)
  {
    if (this._config.cache.threads[threadId] === undefined)
    {
      web.get(`https://www.unknowncheats.me/forum/showthread.php?t=${threadId}`, (data, status) =>
      {
        if (status === 'success')
        {
          let userId = undefined;
          let userReputation = undefined;

          // Member ID
          let start = data.indexOf('https://www.unknowncheats.me/forum/members/');

          if (start !== -1)
          {
            start += 43;
            let end = data.indexOf('.', start);

            let strUserId = data.substring(start, end);

            userId = parseInt(strUserId);
          }

          // Member Reputation
          start = data.indexOf(`_${userId}\">`);

          if (start !== -1)
          {
            start += 3 + userId.toString().length;
            let end = data.indexOf('<', start);

            let strUserReputation = data.substring(start, end);

            userReputation = parseInt(strUserReputation);
          }

          if (userId !== undefined)
          {
            this._config.cache.threads[threadId] = {
              op: {
                id: userId
              }
            };

            if (this._config.cache.members[userId])
            {
              this._config.cache.members[userId].reputation = userReputation;
            }
            else
            {
              this._config.cache.members[userId] = {
                reputation: userReputation
              }
            }

            config.set(this._config);
            callback && callback(undefined, this._config.cache.threads[threadId]);
          }
          else
          {
            callback && callback({ msg: 'unable to find user id' });
          }
        }
        else
        {
          callback && callback({ msg: 'request failed', err: data });
        }
      });
    }
    else
    {
      callback && callback(undefined, this._config.cache.threads[threadId]);
    }
  }

  /**
   * Constructor
   */
  constructor()
  {
    this._config = {};

    config.get((cfg) =>
    {
      this._config = cfg;
    });
  }
}

const cache = new Cache();