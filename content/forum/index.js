class Index
{
  /**
   * Extracts a thread's raw forum name from a URL
   * @param {string} threadURL The thread's URL
   * @return {string} The raw forum name
   */
  _getForumName(threadURL)
  {
    let start = threadURL.indexOf('/forum/') + 7;
    let end = threadURL.indexOf('/', start);

    let forumName = threadURL.substring(start, end);

    return forumName;
  }

  /**
   * Extracts a thread's id from a URL
   * @param {string} threadURL The thread's URL
   * @return {number} The thread's id
   */
  _getThreadId(threadURL)
  {
    let start = threadURL.indexOf('/', threadURL.indexOf('/forum/') + 7) + 1;
    let end = threadURL.indexOf('-', start);

    let strThreadId = threadURL.substring(start, end);
    let threadId = parseInt(strThreadId);

    return threadId;
  }


  /**
   * Gets the member id of the user who posted the given shout
   * @param {object} hrefUser The DOM shout user element
   * @return {number} The member id of the shout owner
   */
  _getShoutUserId(hrefUser)
  {
    for (let attribute of hrefUser.attributes)
    {
      if (attribute.name === 'data-userid')
      {
        return parseInt(attribute.value);
      }
    }

    console.log(`Could not find userId of shout:`);
    console.log(hrefUser);

    return 0;
  }

  /**
   * Updates custom UI elements in the 'Top 10 Stats' Table
   */
  _updateStatsUI()
  {
    let threads = document.querySelector(`#dbtech_topxstats_column3 > table > tbody`).children;

    for (let entry of threads)
    {
      let tdForum = entry.children[entry.children.length - 1];

      let aForum = tdForum.children[0];
      let aThread = entry.querySelector('a');

      if (!aForum)
      {
        // Stats are still loading
        continue;
      }

      {
        let forumName = this._getForumName(aForum.href);

        let btnForum = tdForum.querySelector('.blacklist_forum');

        if (btnForum)
        {
          let isForumBlacklisted = this._blacklist.isForumBlacklisted(forumName);

          btnForum.title = localize[isForumBlacklisted ? 'remove_blacklist_forum' : 'add_blacklist_forum'];
          btnForum.style.backgroundImage = `url(\"${chrome.extension.getURL(`../resources/icons/blacklist_${isForumBlacklisted ? 'remove' : 'add'}.svg`)}\")`;
        }
      }

      {
        let btnThread = entry.querySelector('.blacklist_thread');

        if (btnThread)
        {
          let threadId = this._getThreadId(aThread.href);

          let isThreadBlacklisted = this._blacklist.isThreadBlacklisted(threadId);

          btnThread.title = localize[isThreadBlacklisted ? 'remove_blacklist_thread' : 'add_blacklist_thread'];
          btnThread.style.backgroundImage = `url(\"${chrome.extension.getURL(`../resources/icons/blacklist_${isThreadBlacklisted ? 'remove' : 'add'}.svg`)}\")`;
        }
      }
    }
  }

  /**
   * Updates custom UI elements in the shoutbox
   */
  _updateShoutboxUI()
  {
    let shouts = document.getElementsByClassName('dbtech_vbshout_shout');

    for (let shout of shouts)
    {
      let btn = shout.querySelector('button');

      if (btn)
      {
        let hrefUser = shout.querySelector('span > div > a');

        if (hrefUser)
        {
          let userId = this._getShoutUserId(hrefUser);

          let isMemberBlacklisted = this._blacklist.isMemberBlacklisted(userId);

          btn.title = localize[isMemberBlacklisted ? 'remove_blacklist_member' : 'add_blacklist_member'];
          btn.style.backgroundImage = `url(\"${chrome.extension.getURL(`../resources/icons/blacklist_${isMemberBlacklisted ? 'remove' : 'add'}.svg`)}\")`;
        }
      }
    }
  }

  /**
   * Updates custom UI elements.
   * This needs to be called after filters have been modified to apply the
   * changes to all controls responsible for that filter.
   */
  _updateUI()
  {
    this._updateStatsUI();

    this._updateShoutboxUI();
  }

  /**
   * Adds custom UI elements to the 'Top 10 Stats' table
   */
  _createStatsUI()
  {
    let threads = document.querySelector(`#dbtech_topxstats_column3 > table > tbody`).children;

    if (threads.length !== 10)
    {
      // Stats are still loading, give it some time

      setTimeout(() => { this._createUI.apply(this); }, 500);
      return;
    }

    for (let entry of threads)
    {
      let tdForum = entry.children[entry.children.length - 1];

      if (!tdForum.querySelector('.blacklist_forum'))
      {
        let aForum = tdForum.children[0];
        let aThread = entry.querySelector('a');

        let forumName = this._getForumName(aForum.href);
        let threadId = this._getThreadId(aThread.href);

        // Blacklist forum
        {
          let btn = document.createElement('button');
          btn.className = 'blacklist_forum';

          btn.onclick = (ev) =>
          {
            let isForumBlacklisted = this._blacklist.isForumBlacklisted(forumName);

            if (isForumBlacklisted)
            {
              this._blacklist.removeForum(forumName);
            }
            else
            {
              this._blacklist.addForum(forumName);
            }

            this._updateUI();
          };

          tdForum.appendChild(btn);
        }

        // Blacklist thread
        {
          let btn = document.createElement('button');
          btn.className = 'blacklist_thread';

          btn.onclick = (ev) =>
          {
            let isThreadBlacklisted = this._blacklist.isThreadBlacklisted(threadId);

            if (isThreadBlacklisted)
            {
              this._blacklist.removeThread(threadId);
            }
            else
            {
              this._blacklist.addThread(threadId);
            }

            this._updateUI();
          };

          entry.insertBefore(btn, entry.children[0]);
        }
      }
    }
  }

  /**
   * Adds custom UI elements to the shoutbox
   */
  _createShoutboxUI()
  {
    let shouts = document.getElementsByClassName('dbtech_vbshout_shout');

    for (let shout of shouts)
    {
      let hrefUser = shout.querySelector('span > div > a');

      if (hrefUser)
      {
        let memberId = this._getShoutUserId(hrefUser);

        if (!shout.querySelector('.blacklist_member'))
        {
          let btn = document.createElement('button');
          btn.className = 'blacklist_member';

          btn.onclick = (ev) =>
          {
            let isMemberBlacklisted = this._blacklist.isMemberBlacklisted(memberId);

            if (isMemberBlacklisted)
            {
              this._blacklist.removeMember(memberId);
            }
            else
            {
              this._blacklist.addMember(memberId);
            }

            this._updateUI();
          };

          shout.insertBefore(btn, shout.children[2]);
        }
      }
    }
  }

  /**
   * Adds custom UI elements to control the addon
   */
  _createUI()
  {
    this._createStatsUI();

    this._createShoutboxUI();

    this._updateUI();

    setTimeout(() => { this._createUI.apply(this); }, 1000);
  }

  /**
   * Modifies a shoutbox entry which was caught by a filter
   * @param {object} shout The btech_vbshout_shout DOM object
   */
  _filterShout(shout)
  {
    shout.style.opacity = '0.2';
  }

  /**
   * Restores a shout's default style
   * @param {object} shout The btech_vbshout_shout DOM object
   */
  _unfilterShout(shout)
  {
    shout.style.opacity = '1.0';
  }

  /**
   * Filters shoutbox messages
   */
  _runShoutboxFilter()
  {
    let shouts = document.getElementsByClassName('dbtech_vbshout_shout');

    for (let shout of shouts)
    {
      let hrefUser = shout.querySelector('span > div > a');

      if (hrefUser)
      {
        let userId = this._getShoutUserId(hrefUser);

        if (this._blacklist.isMemberBlacklisted(userId))
        {
          this._filterShout(shout);
        }
        else
        {
          this._unfilterShout(shout);
        }
      }
    }

    setTimeout(() => { this._runShoutboxFilter.apply(this); }, 1000);
  }

  /**
   * Modifies a 'Top 10 Stats' 'Latest Posts' entry which was caught by a filter
   * @param {object} post The tr DOM object
   */
  _filterStatsThread(post)
  {
    post.style.opacity = '0.2';
  }

  /**
   * Restores a 'Top 10 Stats' 'Latest Posts' entry's default style
   * @param {object} post The tr DOM object
   */
  _unfilterStatsThread(post)
  {
    post.style.opacity = '1.0';
  }

  /**
   * Filters a specific column in the 'Top 10 Stats' overview
   * @param {number} column The column to filter
   */
  _applyTopXStatsFilter(column)
  {
    let threads = document.querySelector(`#dbtech_topxstats_column${column} > table > tbody`).children;

    for (let entry of threads)
    {
      let hrefThread = entry.querySelector('td > a');

      if (hrefThread)
      {
        let threadURL = hrefThread.href;

        let forumName = this._getForumName(threadURL);
        let threadName = hrefThread.title;
        let threadId = this._getThreadId(threadURL);

        let shouldThreadBeRemoved = false;

        let qmCount = (threadName.split('?').length - 1);
        if (qmCount > this._config.filter.maximumQuestionMarks)
        {
          shouldThreadBeRemoved = true;
        }

        if (!shouldThreadBeRemoved)
        {
          if (this._blacklist.isThreadBlacklisted(threadId))
          {
            shouldThreadBeRemoved = true;
          }
        }

        if (!shouldThreadBeRemoved)
        {
          if (this._blacklist.isForumBlacklisted(forumName))
          {
            shouldThreadBeRemoved = true;
          }
        }

        if (!shouldThreadBeRemoved)
        {
          cache.getThread(threadId, (err, thread) =>
          {
            if (err)
            {
              console.log(`unable to get thread ${threadId} (https://www.unknowncheats.me/forum/showthread.php?t=${threadId}):`);
              console.log(err);
            }
            else
            {
              if (this._blacklist.isMemberBlacklisted(thread.op.id))
              {
                shouldThreadBeRemoved = true;
              }

              if (!shouldThreadBeRemoved)
              {
                cache.getMember(thread.op.id, (err, member) =>
                {
                  if (err)
                  {
                    console.log(`unable to get member ${thread.op.id} (https://www.unknowncheats.me/forum/members/${thread.op.id}.html) (op of ${threadURL})`);
                    console.log(err);
                  }
                  else
                  {
                    if (member.reputation < this._config.filter.minimumReputation)
                    {
                      shouldThreadBeRemoved = true;
                    }
                  }
                });
              }
            }
          });
        }

        if (shouldThreadBeRemoved)
        {
          this._filterStatsThread(entry);
        }
        else
        {
          this._unfilterStatsThread(entry);
        }
      }
    }
  }

  /**
   * Filters all 'Top 10 Stats' columns
   */
  _runStatsFilter()
  {
    // Hottest Threads
    this._applyTopXStatsFilter(1);

    // Latest Posts
    this._applyTopXStatsFilter(3);

    setTimeout(() => { this._runStatsFilter.apply(this); }, 1000);
  }

  /**
   * Constructor
   */
  constructor()
  {
    this._config = {};

    this._blacklist = undefined;

    config.get((cfg) =>
    {
      this._config = cfg;

      this._blacklist = new Blacklist(this._config);

      this._createUI();

      this._runShoutboxFilter();
      this._runStatsFilter();
    });
  }
}

new Index();