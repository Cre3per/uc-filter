class ShowThread
{
  /**
   * Modifies a Post which was caught by a filter
   * @param {object} post The table DOM object
   */
  _filterPost(post)
  {
    // Don't set the opacity too low here, what the user wrote might be autistic
    // but could still be relevant for the thread.
    post.style.opacity = '0.5';
  }

  /**
   * Restores a posts default style
   * @param {object} post The table DOM object
   */
  _unfilterPost(post)
  {
    post.style.opacity = '1.0';
  }

  /**
   * 
   * @param {object} post The posts table
   * @return {number} The member id of the user who submitted this post
   */
  _getPostMember(post)
  {
    let hrefMember = post.querySelector('.bigusername').href;

    if (hrefMember)
    {
      let start = hrefMember.indexOf('/members/') + 9;
      let end = hrefMember.indexOf('.', start);

      let strMemberId = hrefMember.substring(start, end);

      return parseInt(strMemberId);
    }
    else
    {
      console.log(`Unable to find member id of post`);
      console.log(post);

      return undefined;
    }
  }

  /**
   * 
   * @param {object} post The posts table
   * @return {number} The reputation of the member who submitted this post
   */
  _getPostReputation(post)
  {
    let repinfoRep = post.querySelector('span[id^=\"repinfoRep_\"]');

    if (repinfoRep)
    {
      let strReputation = repinfoRep.innerHTML;

      if (strReputation)
      {
        return parseInt(strReputation);
      }
      else
      {
        console.log(`Unable to find reputation of post`);
        console.log(post);

        return undefined;
      }
    }
  }

  /**
   * @return {number} id of the current thread
   */
  _getThreadId()
  {
    let url = window.location.pathname;

    let start = url.indexOf('/forum/') + 7;
    start = url.indexOf('/', start) + 1;

    let end = url.indexOf('-', start);

    let strThreadId = url.substring(start, end);

    return parseInt(strThreadId);
  }

  /**
   * Updates custom UI elements
   */
  _updateUI()
  {
    let posts = document.querySelectorAll('table[id^=\"post\"');

    for (let post of posts)
    {
      let btn = post.querySelector('button');

      if (btn)
      {
        let memberId = this._getPostMember(post);

        let isMemberBlacklisted = this._blacklist.isMemberBlacklisted(memberId);

        btn.title = localize[isMemberBlacklisted ? 'remove_blacklist_member' : 'add_blacklist_member'];
        btn.style.backgroundImage = `url(\"${chrome.extension.getURL(`../resources/icons/blacklist_${isMemberBlacklisted ? 'remove' : 'add'}.svg`)}\"`;
      }
    }
  }

  /**
   * Creates custom UI elements for the thread
   */
  _createThreadUI()
  {
    let title = document.querySelector('div[id^=\"edit\"').querySelector('strong').parentElement;

    if (title)
    {
      let btn = document.createElement('button');
      btn.className = 'blacklist_thread';

      let threadId = this._getThreadId();

      let updateThreadButton = () =>
      {
        let isThreadBlacklisted = this._blacklist.isThreadBlacklisted(threadId);

        btn.title = localize[isThreadBlacklisted ? 'remove_blacklist_thread' : 'remove_blacklist_thread'];
        btn.style.backgroundImage = `url(\"${chrome.extension.getURL(`../resources/icons/blacklist_${isThreadBlacklisted ? 'remove' : 'add'}.svg`)}\")`;
      };

      updateThreadButton();

      btn.onclick = (ev) =>
      {
        let isThreadBlacklisted = this._blacklist.isThreadBlacklisted(threadId);

        if (isThreadBlacklisted)
        {
          this._blacklist.removeThread(threadId);
        }
        else
        {
          this._blacklist.addThread(threadId);
        }

        updateThreadButton();
      };

      title.insertBefore(btn, title.children[0]);
    }
  }

  /**
   * Creates custom UI elements in each post
   */
  _createPostsUI()
  {
    let posts = document.querySelectorAll('table[id^=\"post\"');

    for (let post of posts)
    {
      let btn = post.querySelector('.blacklist_member');

      if (!btn)
      {
        btn = document.createElement('button');
        btn.className = 'blacklist_member';

        btn.onclick = (ev) =>
        {
          let memberId = this._getPostMember(post);

          let isMemberBlacklisted = this._blacklist.isMemberBlacklisted(memberId);

          if (isMemberBlacklisted)
          {
            this._blacklist.removeMember(memberId);
          }
          else
          {
            this._blacklist.addMember(memberId);
          }

          this._updateUI();
        };

        let tbody = post.children[0];
        let tr = tbody.children[1];
        let td = tr.children[0];
        let div = td.children[0];
        let center = div.children[0];

        center.insertBefore(btn, center.children[0]);
      }
    }
  }

  /**
   * Creates custom UI elements
   */
  _createUI()
  {
    this._createPostsUI();

    this._updateUI();

    setTimeout(() => { this._createPostsUI.apply(this); }, 1000);
  }

  /**
   * Parses the active page and saves all users in the members cache
   */
  _cacheMembers()
  {
    let posts = document.querySelectorAll('table[id^=\"post\"');

    for (let post of posts)
    {
      let memberId = this._getPostMember(post);

      if (memberId)
      {
        let memberReputation = this._getPostReputation(post);

        if (!this._config.cache.members[memberId])
        {
          this._config.cache.members[memberId] = {};
        }

        this._config.cache.members[memberId].reputation = memberReputation;
      }
    }

    config.set(this._config);
  }

  /**
   * Filters posts 
   */
  _runPostsFilter()
  {
    let posts = document.querySelectorAll('table[id^=\"post\"');

    for (let post of posts)
    {
      let shouldFilterPost = false;
      let memberId = this._getPostMember(post);

      if (memberId)
      {
        if (this._blacklist.isMemberBlacklisted(memberId))
        {
          shouldFilterPost = true;
        }

        if (!shouldFilterPost)
        {
          let reputation = this._getPostReputation(post);

          if (reputation < this._config.filter.minimumReputation)
          {
            shouldFilterPost = true;
          }
        }
      }

      if (shouldFilterPost)
      {
        this._filterPost(post);
      }
      else
      {
        this._unfilterPost(post);
      }
    }

    setTimeout(() => { this._runPostsFilter.apply(this); }, 1000);
  }

  /**
   * Constructor
   */
  constructor()
  {
    this._config = {};

    this._blacklist = undefined;

    config.get((cfg) =>
    {
      this._config = cfg;

      this._blacklist = new Blacklist(this._config);

      this._createUI();
      this._createThreadUI();

      this._cacheMembers();
      this._runPostsFilter();
    });
  }
}

new ShowThread();