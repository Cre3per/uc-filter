class Blacklist
{
  /**
   * Checks if the given forum is blacklisted
   * @param {string} name Forum name
   * @return {boolean} True if blacklisted
   */
  isForumBlacklisted(name)
  {
    return this._config.filter.blacklists.forums.includes(name);
  }

  /**
   * Checks if the given member is blacklisted
   * @param {string} id Member id
   * @return {boolean} True if blacklisted
   */
  isMemberBlacklisted(id)
  {
    return this._config.filter.blacklists.members.includes(id);
  }

  /**
   * Checks if the given thread is blacklisted
   * @param {string} id Thread id
   * @return {boolean} True if blacklisted
   */
  isThreadBlacklisted(id)
  {
    return this._config.filter.blacklists.threads.includes(id);
  }

  /**
   * Adds a forum to the blacklist
   * @param {string} name Forum name
   */
  addForum(name)
  {
    if (!this.isForumBlacklisted(name))
    {
      this._config.filter.blacklists.forums.push(name);
      config.set(this._config);
    }
  }

  /**
   * Adds a member to the blacklist
   * @param {number} id Member id
   */
  addMember(id)
  {
    if (!this.isMemberBlacklisted(name))
    {
      this._config.filter.blacklists.members.push(id);
      config.set(this._config);
    }
  }

  /**
   * Adds a thread to the blacklist
   * @param {number} id Thread name
   */
  addThread(id)
  {
    if (!this.isThreadBlacklisted(id))
    {
      this._config.filter.blacklists.threads.push(id);
      config.set(this._config);
    }
  }

  /**
   * Removes a forum from the blacklist
   * @param {string} name Forum name
   */
  removeForum(name)
  {
    let index = this._config.filter.blacklists.forums.indexOf(name);

    if (index !== -1)
    {
      this._config.filter.blacklists.forums.splice(index, 1);
      config.set(this._config);
    }
  }

  /**
   * Removes a member from the blacklist
   * @param {number} id Member id
   */
  removeMember(id)
  {
    let index = this._config.filter.blacklists.members.indexOf(id);

    if (index !== -1)
    {
      this._config.filter.blacklists.members.splice(index, 1);
      config.set(this._config);
    }
  }

  /**
   * Removes a thread from the blacklist
   * @param {number} id Thread name
   */
  removeThread(id)
  {
    let index = this._config.filter.blacklists.threads.indexOf(id);

    if (index !== -1)
    {
      this._config.filter.blacklists.threads.splice(index, 1);
      config.set(this._config);
    }
  }

  /**
   * Constructor
   * @param {object} config The configuration object of the active script
   */
  constructor(config)
  {
    console.assert(config);

    this._config = config;
  }
}