class Config
{
  get(callback)
  {
    chrome.runtime.sendMessage({ 'config': { 'get': true } }, (config) =>
    {
      callback && callback(config);
    });
  }

  set(config, callback)
  {
    chrome.runtime.sendMessage({ 'config': { 'set': config } }, () =>
    {
      callback && callback();
    });
  }
}

const config = new Config();