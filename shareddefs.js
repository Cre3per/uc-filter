const links = {
  git: 'https://gitlab.com/Cre3per/uc-filter',
  update: 'https://gitlab.com/Cre3per/uc-filter/unknowncheatsfilter',
  reportBug: 'https://gitlab.com/Cre3per/uc-filter/issues/new',
  requestFeature: 'https://gitlab.com/Cre3per/uc-filter/issues/new'
};

const localize = {

  // Blacklist
  'add_blacklist_forum': 'Blacklist Forum',
  'add_blacklist_member': 'Blacklist User',
  'add_blacklist_thread': 'Blacklist Thread',
  'remove_blacklist_forum': 'Remove Forum from Blacklist',
  'remove_blacklist_member': 'Remove Member from Blacklist',
  'remove_blacklist_thread': 'Remove Thread from Blacklist',

  '': undefined
};