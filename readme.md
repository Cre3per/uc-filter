# UnknownCheats Filter

<sub><sup>Not affiliated with UnknownCheats™</sup></sub>

---

## Permissions

Storage

- The browsers local storage is used to save the addon's configuration and cache.

https://gitlab.com/Cre3per/uc-filter/raw/master/manifest.json

- Used to check for available updates

## Installation Instructions

Firefox
- Download [unknowncheatsfilter.xpi](https://gitlab.com/Cre3per/uc-filter/raw/master/unknowncheatsfilter.xpi)
- Open [about:addons](about:addons)
- Drag and Drop the downloaded File into Firefox

Chrome
- Download [unknowncheatsfilter.crx](https://gitlab.com/Cre3per/uc-filter/raw/master/unknowncheatsfilter.crx)
- Open [chrome://extensions](chrome://extensions)
- Drag and Drop the downloaded File into Chrome

## Features

- Blacklist Forums
- Blacklist Members
- Blacklist Threads
- Hide threads created by Users with poor Reputation
- Hide threads with too many Question Marks in their Title

## Images

Menu

![menu](menupreview.png)

Shoutbox

![shoutbox](shoutboxpreview.png)

Thread

![thread](threadpreview.png)

Top 10 Stats

![stats](statspreview.png)

## Known Bugs

The addon will load all threads in the _Top 10 Stats_ view, this will cause the contained threads to be marked as read by vBulletin.

After some time idling on a page an uncaught error (`Invocation of form runtime.connect(null, ) doesn't match definition runtime.connect(optional string extensionId, optional object connectInfo)`) will appear. I don't know what's causing this.

<br><br><br>
