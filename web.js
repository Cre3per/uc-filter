class Web
{
  /**
   * @callback Web~getCallback
   * @param {object} data The data returned by the server or an error object
   * @param {string} status Status text
   */
  /**
   * Runs a GET request
   * @param {string} url Target URL
   * @param {Web~getCallback} callback
   */
  get(url, callback)
  {
    let xhr = new XMLHttpRequest();

    xhr.onreadystatechange = () =>
    {
      if (xhr.readyState === XMLHttpRequest.DONE)
      {
        if (xhr.status === 200)
        {
          callback && callback(xhr.responseText, 'success');
        }
        else
        {
          callback && callback(xhr, 'error');
        }
      }
    };

    xhr.open('GET', url, true);
    xhr.send(null);
  }
}

const web = new Web();