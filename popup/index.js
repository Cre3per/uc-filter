class PopUp
{
  /**
   * Opens a URL in the browser
   * @param {string} url The URL to open
   */
  _openURL(url)
  {
    chrome.tabs.create({ url });
  }

  /**
   * Sets GUI options based on the config
   */
  _loadFromConfig()
  {
    config.get((cfg) =>
    {
      console.log(cfg);
      if (cfg.isNewVersionAvailable)
      {
        document.getElementById('update').style.display = 'block';
      }

      let qm = document.getElementById('filterQM');
      qm.value = cfg.filter.maximumQuestionMarks;


      let rep = document.getElementById('filterRep');
      rep.value = cfg.filter.minimumReputation;
    });
  }

  /**
   * Saves GUI options to the config
   */
  _saveToConfig()
  {
    config.get((cfg) =>
    {
      cfg.filter.maximumQuestionMarks = parseInt(document.getElementById('filterQM').value);
      cfg.filter.minimumReputation = parseInt(document.getElementById('filterRep').value);

      config.set(cfg, () =>
      {

      });
    });
  }

  /**
   * Assigns actions to GUI elements
   */
  _assignClickActions()
  {
    document.getElementById('update').onclick = () =>
    {
      this._openURL(`${links.update}.${(typeof(browser) === 'undefined') ? 'crx' : 'xpi'}`);
    };

    document.getElementById('reportBug').onclick = () =>
    {
      this._openURL(links.reportBug);
    };

    document.getElementById('requestFeature').onclick = () =>
    {
      this._openURL(links.reportBug);
    };

    document.getElementById('gitlab').onclick = () =>
    {
      this._openURL(links.git);
    };

    document.getElementById('filterQM').onchange = (ev) =>
    {
      this._saveToConfig();
    };

    document.getElementById('filterRep').onchange = (ev) =>
    {
      this._saveToConfig();
    };
  }

  /**
   * Callback for when the popup has been loaded
   */
  onReady()
  {
    // Chromium adds random whitespace, this seems to fix it
    if (typeof(browser) === 'undefined')
    {
      document.body.style.height = '1px';
      document.children[0].style.height = '1px';
    }

    this._loadFromConfig();
    this._assignClickActions();
  }

  /**
   * Waits for the popup to be loaded. Then calls {@link onReady}
   */
  waitForLoad()
  {
    if (document.readyState === 'complete')
    {
      this.onReady();
    }
    else
    {
      setTimeout(this.waitForLoad.bind(this), 10);
    }
  }

  /**
   * Default constructor
   */
  constructor()
  {
    this.waitForLoad();
  }
}

new PopUp();